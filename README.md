Steps to run this project:

1. Run `npm i` command
2. Setup database settings inside `ormconfig.json` file
3. Run `npm start` command

Quick Start to  TypeORM in TypeScript Guide @ https://www.infoq.com/articles/typescript-mysql/

Next Step : How to connect to remote DB server?

https://stackoverflow.com/questions/10309540/connect-to-sql-server-database-from-node-js
https://github.com/zachwhitedev/barebones-node-mysql


------------------<READ HERE>--------------------

To run these files, please replace the index.ts and User.ts file with the new index.ts and User.ts files.

please keep a backup of those previous files in a safe local location, in case we have to go back for any reason.

please feel free to modify the code asper the project requirements.  :)
