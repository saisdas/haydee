import "reflect-metadata";
import {createConnection} from "typeorm";
import {Fachs, User} from "./entity/User";

createConnection().then(async connection => {

    console.log("Inserting a new user into the database...");
    const driver = new Fachs();
    driver.name = "Joe";
    await connection.manager.save(driver);
    console.log("Saved a new user with id: " + driver.id);

    console.log("Loading users from the database...");
    const users = await connection.manager.find(User);
    console.log("Loaded users: ", driver);

    console.log("Here you can setup and run express/koa/any other framework.");

}).catch(error => console.log(error));
