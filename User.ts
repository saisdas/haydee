import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    firstName: string;

    @Column()
    lastName: string;

    @Column()
    age: number;
    name: string;
}


@Entity()
export class Fachs {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;
}


@Entity()
export class Mentor_Fach {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    Mentor_id: number;

    @Column()
    Fach_id: string;
}


@Entity()
export class Mentee_Fach {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    Mentee_id: number;

    @Column()
    Fach_id: string;
}


@Entity()
export class mentors {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_id: number;

    @Column()
    geburtsdatum: Date;

    @Column()
    Grundschule: string;

    @Column()
    Schulform_Mittelstufe: string;

    @Column()
    Schulform_Oberstufe: string;

    @Column()
    Abitur: boolean;

    @Column()
    Ausbildung: boolean;

    @Column()
    Hoechster_Bildungsabschluss: string;
}


@Entity()
export class mentee {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    user_id: number;

    @Column()
    gender: boolean;

    @Column()
    Schulform: string;

    @Column()
    Klasse: number;

    @Column()
    Muttersprache: string;

    @Column()
    hobby: string;

    @Column()
    gehoertvon: string;

    @Column()
    Formular_ausfuellen: boolean;

    @Column()
    GeraetorApp: string;
}


@Entity()
export class matching {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    Mentor_id: number;

    @Column()
    mentee_id: number;

    @Column()
    rating: number;

    @Column()
    review_body: string;
}


@Entity()
export class users {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    Zeitstempel: Date;

    @Column()
    name: string;

    @Column()
    email: string;

    @Column()
    handynummer: number;
}